// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.2.1" apply false
    id("org.jetbrains.kotlin.android") version "1.9.0" apply false
    id("com.android.library") version "8.2.1" apply false
    id("org.jetbrains.kotlin.jvm") version "1.9.0" apply false
    id("com.google.dagger.hilt.android") version "2.49" apply false
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.0" apply false
}

subprojects {

    plugins.withType<com.android.build.gradle.BasePlugin>().configureEach {
        dependencies {
            add("implementation", "com.jakewharton.timber:timber:5.0.1")
        }
    }
}
