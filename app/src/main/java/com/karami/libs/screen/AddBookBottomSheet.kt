package com.karami.libs.screen

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.karami.domain.books.AddBook

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddBookBottomSheet(
    onSubmitClick: (AddBook) -> Unit
) {
    var addBook = AddBook()
    var errorToast by remember {
        mutableStateOf(false)
    }
    ModalBottomSheet(onDismissRequest = {

    }) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            BookFieldItem(title = "Title", onValueChange = {
                addBook = addBook.copy(title = it)

            })

            BookFieldItem(title = "Author", onValueChange = {
                addBook = addBook.copy(author = it)

            })

            BookFieldItem(title = "Genre", onValueChange = {
                addBook = addBook.copy(genre = it)
            })

            BookFieldItem(title = "Year Published", onValueChange = {
                addBook = addBook.copy(yearPublished = it.toIntOrNull())
            })
            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = {
                    if (addBook.author != null && addBook.genre != null && addBook.title != null && addBook.yearPublished != null)
                        onSubmitClick(addBook)
                    else
                        errorToast = true
                }) {
                Text(text = "Add Book")
            }

            if (errorToast){
                Toast.makeText(LocalContext.current, "Please fill all of blank", Toast.LENGTH_SHORT)
                    .show()
                errorToast = false
            }
        }
    }
}

@Composable
fun BookFieldItem(
    title: String,
    onValueChange: (String) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = "$title: ")
        var value by remember {
            mutableStateOf("")
        }
        TextField(
            value = value,
            onValueChange = {
                value = it
                onValueChange(it)
            }
        )
    }
}