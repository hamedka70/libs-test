package com.karami.libs.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.arkivanov.mvikotlin.extensions.coroutines.stateFlow
import com.karami.domain.books.Book
import com.karami.libs.screen.store.BooksViewModel
import com.karami.libs.R
import com.karami.libs.ThemeHelper
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import com.karami.libs.screen.store.MainStore
import kotlinx.coroutines.ExperimentalCoroutinesApi


@OptIn(ExperimentalMaterial3Api::class, ExperimentalCoroutinesApi::class)
@Composable
fun BooksScreen(
    refresh: Boolean,
    viewModel: BooksViewModel = hiltViewModel(),
    onBookClick: (id: String) -> Unit
) {
    val state by viewModel.store.stateFlow.collectAsState()
    if (refresh) {
        viewModel.store.accept(MainStore.Intent.RefreshList)
    }
    var showAddBookBottomSheet by remember {
        mutableStateOf(false)
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer
                ),
                title = {
                    Text(text = "Books")
                },
                navigationIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.library),
                        tint = Color.Unspecified,
                        contentDescription = "darak mode toggle",
                        modifier = Modifier
                            .size(44.dp)
                            .padding(horizontal = 8.dp)
                    )
                },
                actions = {
                    Row {
                        IconButton(onClick = {
                            showAddBookBottomSheet = !showAddBookBottomSheet
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.add_box),
                                contentDescription = "add book",
                                tint = if (ThemeHelper.getIsDarkMode()) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.error
                            )
                        }

                        IconButton(onClick = {
                            ThemeHelper.toggle()
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.dark_mode),
                                contentDescription = "darak mode",
                                tint = if (ThemeHelper.getIsDarkMode()) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.error
                            )
                        }
                    }
                }
            )
        }
    )
    {
        when (val mState = state.listState) {
            is MainStore.State.ListState.Data ->
                BookList(
                    mState = mState.books,
                    paddingValues = it,
                    onBookClick = onBookClick
                )

            MainStore.State.ListState.Empty -> {}
            MainStore.State.ListState.Error -> NetworkErrorScreen(viewModel = viewModel)
        }
    }

    if (showAddBookBottomSheet) {
        AddBookBottomSheet {
            showAddBookBottomSheet = false
            viewModel.store.accept(MainStore.Intent.AddNewBook(it))
        }
    }
}

    @Composable
    fun BookList(
        mState: List<Book>,
        paddingValues: PaddingValues,
        onBookClick: (id: String) -> Unit
    ) {
        LazyColumn(modifier = Modifier
            .fillMaxWidth()
            .padding(paddingValues), content = {
            items(mState) {
                Book(book = it, onBookClick = onBookClick)
            }
        })
    }

    @Composable
    fun Book(book: Book, onBookClick: (id: String) -> Unit) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .height(100.dp)
                .clickable {
                    onBookClick(book.id ?: "-1")
                }
                .background(if (book.checkedOut == true) MaterialTheme.colorScheme.errorContainer else MaterialTheme.colorScheme.background)
        ) {
            Image(
                painterResource(id = R.drawable.book),
                contentDescription = "book",
                modifier = Modifier.size(60.dp)
            )
            Spacer(modifier = Modifier.size(8.dp))
            Column(
                modifier = Modifier.fillMaxHeight(),
                verticalArrangement = Arrangement.SpaceEvenly
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = "Title:  ",
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = book.title ?: "",
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        style = MaterialTheme.typography.labelMedium
                    )
                }
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = "Author:  ",
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = book.author ?: "",
                        style = MaterialTheme.typography.labelMedium
                    )
                }
            }
            Spacer(modifier = Modifier.size(4.dp))

        }
    }


    @Composable
    fun NetworkErrorScreen(viewModel: BooksViewModel) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {

            Icon(
                painter = painterResource(id = R.drawable.wifi_off),
                contentDescription = "connection error",
                tint = MaterialTheme.colorScheme.error,
                modifier = Modifier.size(120.dp)
            )

            Spacer(modifier = Modifier.size(8.dp))

            Text(
                text = "Check youre internet!",
                style = MaterialTheme.typography.titleLarge
            )

            Spacer(modifier = Modifier.size(24.dp))

            Button(onClick = {
                viewModel.refreshPage()
            }) {
                Text(text = "Refresh...")
            }
        }
    }