package com.karami.libs.screen.store

import com.arkivanov.mvikotlin.core.store.Reducer
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.core.utils.ExperimentalMviKotlinApi
import com.arkivanov.mvikotlin.extensions.coroutines.CoroutineExecutor
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.karami.domain.books.Book
import com.karami.domain.books.BooksRepository
import com.karami.domain.books.ResponseModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

interface DetailStore {
    data class State(
        val bookDetail: Book?
    )

    sealed interface Intent {
        data class GetDetail(val id: String) : Intent
        data class RefreshDetail(val id: String) : Intent

        data class Delete(val id: String): Intent

        data class UpdateCheckOut(val id: String ,val checkOut: Boolean): Intent
    }

    sealed interface Label {
        data object NetworkError : Label

        data object DeleteSuccess: Label
    }
}

class DetailStoreProvider @Inject constructor(
    private val storeFactory: StoreFactory,
    private val booksRepository: BooksRepository
) : CoroutineExecutor<DetailStore.Intent, DetailStoreProvider.Action, DetailStore.State, DetailStoreProvider.Msg, DetailStore.Label>(),
    Reducer<DetailStore.State, DetailStoreProvider.Msg> {

    sealed interface Action {
        data class GetBook(val id: String) : Action
    }


    override fun executeIntent(intent: DetailStore.Intent, getState: () -> DetailStore.State) {
        when (intent) {
            is DetailStore.Intent.GetDetail -> {
                scope.launch {
                    when (val result = booksRepository.getBook(intent.id)) {
                        is ResponseModel.Error -> publish(DetailStore.Label.NetworkError)
                        is ResponseModel.Success -> result.t?.let { dispatch(Msg.OnGetDetail(it)) }
                    }
                }
            }

            is DetailStore.Intent.RefreshDetail -> scope.launch {
            }

            is DetailStore.Intent.Delete -> scope.launch {
                when(booksRepository.deleteBook(intent.id)){
                    is ResponseModel.Error -> publish(DetailStore.Label.NetworkError)
                    is ResponseModel.Success -> publish(DetailStore.Label.DeleteSuccess)
                }
            }
            is DetailStore.Intent.UpdateCheckOut -> scope.launch {
                when(booksRepository.updateBook(intent.id , intent.checkOut)){
                    is ResponseModel.Error -> publish(DetailStore.Label.NetworkError)
                    is ResponseModel.Success -> executeAction(Action.GetBook(intent.id))
                }

            }
        }
    }

    override fun executeAction(action: Action, getState: () -> DetailStore.State) {
        when (action) {
            is Action.GetBook -> scope.launch{
                when (val result = booksRepository.getBook(action.id)) {
                    is ResponseModel.Error -> publish(DetailStore.Label.NetworkError)
                    is ResponseModel.Success -> result.t?.let { dispatch(Msg.OnGetDetail(it)) }
                }
            }
        }
    }

    @OptIn(ExperimentalMviKotlinApi::class)
    operator fun invoke(id: String): Store<DetailStore.Intent, DetailStore.State, DetailStore.Label> =
        storeFactory.create(
            name = "DetailStoreProvider-$id",
            initialState = DetailStore.State(null),
            executorFactory = { this },
            reducer = this,
            bootstrapper = coroutineBootstrapper {
                this.dispatch(Action.GetBook(id))
            }
        )

    sealed interface Msg {
        data class OnGetDetail(val book: Book) : Msg

    }

    override fun DetailStore.State.reduce(msg: Msg): DetailStore.State {
        return when (msg) {
            is Msg.OnGetDetail -> copy(bookDetail = msg.book)
        }
    }

}


