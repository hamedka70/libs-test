package com.karami.libs.screen

import android.widget.Toast
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.arkivanov.mvikotlin.extensions.coroutines.labels
import com.arkivanov.mvikotlin.extensions.coroutines.stateFlow
import com.karami.domain.books.Book
import com.karami.libs.R
import com.karami.libs.screen.store.DetailStore
import com.karami.libs.screen.store.DetailViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun Detail(
    onBackClick: () -> Boolean,
    onChangeCheckOut: (id: String, checkout: Boolean) -> Unit,
    onDelete: (id: String) -> Unit,
    viewModel: DetailViewModel
) {
    var showToast by remember {
        mutableStateOf(false)
    }
    LaunchedEffect(key1 = Unit, block = {
        viewModel.store.labels.collectLatest {
            when (it) {
                DetailStore.Label.DeleteSuccess -> onBackClick()
                DetailStore.Label.NetworkError -> showToast = true
            }
        }
    })

    if (showToast) {
        Toast.makeText(LocalContext.current, "Network Error ...", Toast.LENGTH_LONG).show()
        showToast = false
    }
    val book =  viewModel.store.stateFlow.collectAsState().value.bookDetail
    var checkoutRemember by remember {
        mutableStateOf(book?.checkedOut ?: false)
    }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .background(if (book?.checkedOut == true) MaterialTheme.colorScheme.errorContainer else MaterialTheme.colorScheme.background)
    ) {
        Spacer(modifier = Modifier.size(8.dp))
        FloatingActionButton(
            onClick = {
                onBackClick()
            }) {
            Image(
                painter = painterResource(id = R.drawable.arrow_back),
                contentDescription = "arrow back"
            )
        }

        Spacer(modifier = Modifier.size(8.dp))
        LazyColumn {

            item {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = book?.title ?: "",
                        style = MaterialTheme.typography.titleLarge
                    )
                }
            }

            item {
                Spacer(modifier = Modifier.size(16.dp))
                Image(
                    painterResource(id = R.drawable.book),
                    contentDescription = null,
                    modifier = Modifier.size(100.dp)
                )
                Spacer(modifier = Modifier.size(16.dp))
            }
            item {
                Spacer(modifier = Modifier.size(8.dp))

            }
            items(
                listOf(
                    "Author :" to book?.author,
                    "Genre :" to book?.genre,
                    "Created At :" to book?.createdAt,
                    "Year Published :" to book?.yearPublished
                )
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = it.first,
                        modifier = Modifier
                            .padding(4.dp),
                        style = MaterialTheme.typography.titleSmall
                    )

                    Text(
                        text = it.second ?: "                       ",
                        modifier = Modifier
                            .padding(vertical = 4.dp)
                            .background(shimmerBrush(showShimmer = it.second == null)),
                        style = MaterialTheme.typography.labelMedium
                    )
                }

            }
        }
        checkoutRemember = book?.checkedOut ?: false
        Switch(
            checked = book?.checkedOut ?: false, onCheckedChange = { check ->
                book?.id?.let {
                    onChangeCheckOut(it, check)
                }
                checkoutRemember = checkoutRemember.not()
            },
            thumbContent = {
                if (checkoutRemember != book?.checkedOut) {
                    CircularProgressIndicator(
                        modifier = Modifier.padding(2.dp),
                        strokeWidth = 2.dp,
                    )
                }
            }
        )

        Button(
            modifier = Modifier.fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.error),
            onClick = {
                book?.id?.let {
                    onDelete(it)
                }
            }) {
            Text(text = "Delete")
        }
    }

}

@Composable
fun shimmerBrush(showShimmer: Boolean = true, targetValue: Float = 1000f): Brush {
    return if (showShimmer) {
        val shimmerColors = listOf(
            Color.LightGray.copy(alpha = 0.6f),
            Color.LightGray.copy(alpha = 0.2f),
            Color.LightGray.copy(alpha = 0.6f),
        )

        val transition = rememberInfiniteTransition(label = "")
        val translateAnimation = transition.animateFloat(
            initialValue = 0f,
            targetValue = targetValue,
            animationSpec = infiniteRepeatable(
                animation = tween(800), repeatMode = RepeatMode.Reverse
            ), label = ""
        )
        Brush.linearGradient(
            colors = shimmerColors,
            start = Offset.Zero,
            end = Offset(x = translateAnimation.value, y = translateAnimation.value)
        )
    } else {
        Brush.linearGradient(
            colors = listOf(Color.Transparent, Color.Transparent),
            start = Offset.Zero,
            end = Offset.Zero
        )
    }
}