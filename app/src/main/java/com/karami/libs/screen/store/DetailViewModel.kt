package com.karami.libs.screen.store

import androidx.lifecycle.ViewModel
import com.arkivanov.mvikotlin.core.store.Store
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class DetailViewModel @Inject constructor(
    private val detailStoreProvider: DetailStoreProvider,
) : ViewModel() {

    lateinit var store: Store<DetailStore.Intent, DetailStore.State, DetailStore.Label>
        private set
    fun init(id: String) {
        if (::store.isInitialized.not()) {
            store = detailStoreProvider(id)
        }
    }

    override fun onCleared() {
        store.dispose()
        super.onCleared()
    }
}