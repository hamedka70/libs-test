package com.karami.libs.screen.store

import com.arkivanov.mvikotlin.core.store.Reducer
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.core.utils.ExperimentalMviKotlinApi
import com.arkivanov.mvikotlin.extensions.coroutines.CoroutineExecutor
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import com.karami.domain.books.AddBook
import com.karami.domain.books.Book
import com.karami.domain.books.BooksRepository
import com.karami.domain.books.ResponseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface MainStore {
    data class State(
        val listState: ListState = ListState.Empty
    ) {
        sealed interface ListState {
            data object Empty: ListState
            data class Data(val books: List<Book>): ListState

            data object Error: ListState
        }
    }
    sealed interface Intent {
        data object RefreshList: Intent
        data class AddNewBook(val addBook: AddBook): Intent
    }
}

class MainStoreProvider @Inject constructor(
    private val storeFactory: StoreFactory,
    private val booksRepository: BooksRepository
) : CoroutineExecutor<MainStore.Intent, MainStoreProvider.Action, MainStore.State, MainStoreProvider.Msg, Nothing>(),
    Reducer<MainStore.State, MainStoreProvider.Msg> {


    override fun executeAction(action: Action, getState: () -> MainStore.State) {
        when (action) {
            Action.GetBooks -> {
                scope.launch {
                    when(val result = withContext(Dispatchers.IO) { booksRepository.getBooks() } ){
                        is ResponseModel.Error -> dispatch(Msg.OnError)
                        is ResponseModel.Success -> dispatch(Msg.OnGetNewList(result.t?: listOf()))
                    }
                }
            }
        }

    }


    override fun executeIntent(intent: MainStore.Intent, getState: () -> MainStore.State) {
        when (intent) {
            MainStore.Intent.RefreshList -> executeAction(Action.GetBooks,getState)
            is MainStore.Intent.AddNewBook -> scope.launch {
                when(booksRepository.addBook(intent.addBook)){
                    is ResponseModel.Error -> dispatch(Msg.OnError)
                    is ResponseModel.Success -> executeAction(Action.GetBooks)
                }
            }
        }
    }

    operator fun invoke(): Store<MainStore.Intent, MainStore.State, Nothing> = storeFactory.create(
        name = "RootStoreProvider",
        initialState = MainStore.State(),
        executorFactory = { this },
        bootstrapper = bootstrapper,
        reducer = this
    )

    @OptIn(ExperimentalMviKotlinApi::class)
    private val bootstrapper by lazy {
        coroutineBootstrapper {
            this.dispatch(Action.GetBooks)
        }
    }

    sealed interface Action {
        data object GetBooks: Action
    }

    sealed interface Msg {
        data class OnGetNewList(val list: List<Book>): Msg

        data object OnError: Msg

    }

    override fun MainStore.State.reduce(msg: Msg): MainStore.State {
        return when (msg) {
            is Msg.OnGetNewList -> copy(
                listState = if (msg.list.isNotEmpty()) {
                    MainStore.State.ListState.Data(msg.list)
                } else {
                    listState
                }

            )

            Msg.OnError -> copy(listState = MainStore.State.ListState.Error)
        }
    }

}