package com.karami.libs.screen.store

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BooksViewModel @Inject constructor(
    private val mainStoreProvider: MainStoreProvider
) : ViewModel() {
    val store by lazy {
        mainStoreProvider()
    }

    fun refreshPage(){
        store.accept(
            MainStore.Intent.RefreshList
        )
    }

    override fun onCleared() {
        store.dispose()
        super.onCleared()
    }



}