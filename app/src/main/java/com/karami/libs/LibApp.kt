package com.karami.libs

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.arkivanov.mvikotlin.extensions.coroutines.stateFlow
import com.karami.libs.screen.BooksScreen
import com.karami.libs.screen.Detail
import com.karami.libs.screen.store.DetailStore
import com.karami.libs.screen.store.DetailViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi


@Composable
fun LibApp(
) {
    val navController = rememberNavController()
    LibNavHost(
        navController = navController
    )
}

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun LibNavHost(
    navController: NavHostController
) {
    NavHost(navController = navController, startDestination = "home") {
        var refreshPage = false
        composable("home") {
            BooksScreen(refreshPage) {
                refreshPage = false
                navController.navigate("detail/${it}")
            }
        }
        composable(
            "detail/{id}",
            arguments = listOf(navArgument("id") {
                type = NavType.StringType
            })
        ) {

            val viewmodel = hiltViewModel<DetailViewModel>()
            it.arguments?.getString("id")?.let {
                viewmodel.init(it)
            }
            Detail(
                onBackClick = { navController.navigateUp() },
                onChangeCheckOut = {id , checkout ->
                    refreshPage = true
                    viewmodel.store.accept(DetailStore.Intent.UpdateCheckOut(id , checkout))
                },
                onDelete = {
                    refreshPage = true
                    viewmodel.store.accept(DetailStore.Intent.Delete(it))
                },
                viewModel = viewmodel
            )
        }
    }
}
