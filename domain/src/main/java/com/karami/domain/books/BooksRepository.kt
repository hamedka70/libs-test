package com.karami.domain.books


interface BooksRepository {
    suspend fun getBooks(): ResponseModel<List<Book>>
    suspend fun getBook(id: String): ResponseModel<Book>
    suspend fun updateBook(id: String , checkout: Boolean): ResponseModel<Nothing>
    suspend fun addBook(addBook: AddBook): ResponseModel<Nothing>
    suspend fun deleteBook(id: String): ResponseModel<Nothing>
}