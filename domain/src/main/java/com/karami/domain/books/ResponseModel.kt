package com.karami.domain.books
sealed class ResponseModel<out T> {
    data class Success<T>(val t: T?): ResponseModel<T>()
    data class Error(val throwable: Throwable?): ResponseModel<Nothing>()
}