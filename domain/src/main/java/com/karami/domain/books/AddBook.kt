package com.karami.domain.books
data class AddBook(
    val title: String? = null,
    val author: String? = null,
    val genre: String? = null,
    val yearPublished: Int? = null
)
