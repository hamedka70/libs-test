package com.karami.domain.books

data class Book(
    val title: String? = null,
    val author: String? = null,
    val id: String? = null,
    val genre: String? = null,
    val yearPublished: String? = null,
    val checkedOut: Boolean? = null,
    val createdAt: String? = null
)