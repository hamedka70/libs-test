package com.karami.repository

import com.karami.domain.books.BooksRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DiModule {
    @Binds
    abstract fun bindBooksRepository(booksRepoImpl: BooksRepoImpl): BooksRepository
}