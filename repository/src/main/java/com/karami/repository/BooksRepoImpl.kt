package com.karami.repository

import com.karami.domain.books.AddBook
import com.karami.domain.books.Book
import com.karami.domain.books.BooksRepository
import com.karami.domain.books.ResponseModel
import com.karami.booksapi.BooksApi
import javax.inject.Inject

class BooksRepoImpl @Inject constructor(
    private val booksApi: BooksApi
) : BooksRepository {
    override suspend fun getBooks(): ResponseModel<List<Book>> {
        val result = booksApi.getBooks()
        return result.getOrNull()?.let {
            ResponseModel.Success(it.map { book ->
                Book(
                    title = book.title,
                    id = book.id,
                    author = book.author,
                    genre = book.genre,
                    yearPublished = book.yearPublished.toString(),
                    checkedOut = book.checkedOut,
                    createdAt = book.createdAt
                )
            })
        } ?: ResponseModel.Error(result.exceptionOrNull())
    }

    override suspend fun getBook(id: String): ResponseModel<Book> {
        val result = booksApi.getBook(id)
        return result.getOrNull()?.let { book ->
            ResponseModel.Success(
                Book(
                    title = book.title,
                    id = book.id,
                    author = book.author,
                    genre = book.genre,
                    yearPublished = book.yearPublished.toString(),
                    checkedOut = book.checkedOut,
                    createdAt = book.createdAt
                )
            )
        } ?: ResponseModel.Error(result.exceptionOrNull())
    }
    override suspend fun updateBook(id: String, checkout: Boolean): ResponseModel<Nothing> {
        val result = booksApi.updateBook(id , checkout)
        return result.getOrNull()?.let {
            ResponseModel.Success(null)
        } ?: ResponseModel.Error(result.exceptionOrNull())
    }
    override suspend fun addBook(addBook: AddBook): ResponseModel<Nothing> {
        val result = booksApi.addBook(
            com.karami.booksapi.model.AddBook(
                title = addBook.title,
                author = addBook.author,
                genre = addBook.genre,
                yearPublished = addBook.yearPublished
            )
        )
        return result.getOrNull()?.let {
            ResponseModel.Success(null)
        } ?: ResponseModel.Error(result.exceptionOrNull())
    }
    override suspend fun deleteBook(id: String): ResponseModel<Nothing> {
        val result = booksApi.deleteBook(id)
        return result.getOrNull()?.let {
            ResponseModel.Success(null)
        } ?: ResponseModel.Error(result.exceptionOrNull())
    }
}