package com.karami.base

object EndPoints {
    const val getBooks = "https://postman-library-api.glitch.me/books"
    const val addBook = "https://postman-library-api.glitch.me/books"
}