package com.karami.base

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.HttpRequestRetry
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logging
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiClient @Inject constructor(
    private val json: Json
) {


    fun getKtorClient(): HttpClient {
        return  HttpClient(OkHttp) {
            install(Logging) {
                logger = ApiLogger.KtorLogger
                level = LogLevel.ALL
            }

            install(ContentNegotiation){
                json(json)
            }

            install(HttpTimeout){
                val timeout = 60 * 1000L
                requestTimeoutMillis = timeout
                socketTimeoutMillis = timeout
                connectTimeoutMillis = timeout
            }

            install(HttpRequestRetry) {
                maxRetries = 3
                retryOnExceptionIf { _, cause ->
                    cause is ClientRequestException && cause.response.status == HttpStatusCode.RequestTimeout
                }
                delayMillis { retry ->
                    retry * 1000L
                } // retries in 1, 2, 3, etc. seconds
            }
            expectSuccess = true


        }
    }

}