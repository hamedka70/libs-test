package com.karami.booksapi

import com.karami.base.ApiClient
import com.karami.base.EndPoints
import com.karami.booksapi.model.AddBook
import com.karami.booksapi.model.Book
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.patch
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import javax.inject.Inject

internal class BooksApiImpl @Inject constructor(
    private val apiClient: ApiClient
) : BooksApi {
    override suspend fun getBooks(): Result<List<Book>> =
        runCatching {
            apiClient.getKtorClient().get(EndPoints.getBooks).body()
        }


    override suspend fun getBook(id: String): Result<Book> = runCatching {
        apiClient.getKtorClient().get(EndPoints.getBooks.plus("/$id")).body()
    }


    override suspend fun deleteBook(id: String) = runCatching {
        apiClient.getKtorClient().delete(EndPoints.getBooks.plus("/$id")).body<String>().let {}
    }


    override suspend fun updateBook(id: String, checkout: Boolean) = runCatching {
        apiClient.getKtorClient().patch(EndPoints.getBooks.plus("/$id")) {
            setBody(
                mapOf(
                    "checkedOut" to checkout
                )
            )
            contentType(ContentType.Application.Json)
        }.let {  }
    }


    override suspend fun addBook(book: AddBook) = runCatching {
        apiClient.getKtorClient().post(EndPoints.addBook) {
            setBody(book)
            contentType(ContentType.Application.Json)
        }.let {  }
    }

}
