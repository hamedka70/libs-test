package com.karami.booksapi.model

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class Book(
    @SerializedName("title") val title: String? = null,
    @SerializedName("author") val author: String? = null,
    @SerializedName("id") val id: String? = null,
    @SerializedName("genre") val genre: String? = null,
    @SerializedName("yearPublished") val yearPublished: Int? = null,
    @SerializedName("checkedOut") val checkedOut: Boolean? = null,
    @SerializedName("createdAt") val createdAt: String? = null

)