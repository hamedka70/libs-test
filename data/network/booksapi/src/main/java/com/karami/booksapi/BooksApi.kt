package com.karami.booksapi

import com.karami.booksapi.model.AddBook
import com.karami.booksapi.model.Book

interface BooksApi {
    suspend fun getBooks(): Result<List<Book>>

    suspend fun getBook(id: String): Result<Book>

    suspend fun deleteBook(id: String): Result<Unit>

    suspend fun updateBook(id: String , checkout: Boolean): Result<Unit>

    suspend fun addBook(book: AddBook): Result<Unit>
}