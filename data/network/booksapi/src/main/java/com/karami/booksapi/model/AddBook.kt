package com.karami.booksapi.model

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class AddBook(
    @SerializedName("title") val title: String? = null,
    @SerializedName("author") val author: String? = null,
    @SerializedName("genre") val genre: String? = null,
    @SerializedName("yearPublished") val yearPublished: Int? = null
)
