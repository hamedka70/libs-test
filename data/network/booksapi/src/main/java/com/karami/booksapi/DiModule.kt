package com.karami.booksapi

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal abstract class DiModule {
    @Binds
    abstract fun bindBooksApi(booksApi: BooksApiImpl): BooksApi
}