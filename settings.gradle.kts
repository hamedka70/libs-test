rootProject.name = "libs-test"
pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")


include(":app")
include(":data")
include(":domain")
include(":repository")
include(":data:network")
include(":data:network:base")
include(":data:network:booksapi")
include(":di")
